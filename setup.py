import os
from distutils.core import setup, Extension


with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="treegen-tstepi",
    version="0.1",
    author="Tomaž Stepišnik",
    author_email="tomaz.stepi@gmail.com",
    description="Evolutionary learning of decision trees for interpretability.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/TStepi/treegen",
    packages=['treegen'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=['numpy', 'pandas', 'joblib', 'scikit-learn', 'tqdm'],
)
