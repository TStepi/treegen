import pandas as pd
import numpy as np
from itertools import combinations, chain


class Test:
    def __init__(self, feature_idx, is_nominal, condition):
        self.feature = feature_idx
        self.is_nominal = is_nominal
        self.condition = condition


    def apply(self, x):
        val = x[self.feature]
        if pd.isna(val):
            return 0.5
        if self.is_nominal:
            return int(val in self.condition)
        else:
            return int(val < self.condition)

    def repr(self, feature_names):
        if self.is_nominal:
            return f'{feature_names[self.feature]} in {self.condition}?'
        else:
            return f'{feature_names[self.feature]} < {self.condition}?'


class TestGenerator:
    def __init__(self, X, nominal_features, allow_subsets):
        self.possible_tests = []
        self.non_constant_features = []
        for i in range(X.shape[1]):
            if i in nominal_features:
                values = set(X[:, i])
                if allow_subsets:
                    # All subsets of possible values
                    subsets = chain.from_iterable(combinations(values, r) for r in range(1, len(values)))
                    self.possible_tests.append([
                        Test(i, True, s) for s in subsets
                    ])
                else:
                    # Only individual values are used
                    self.possible_tests.append([
                        Test(i, True, (v,)) for v in values
                    ])
            else:
                # All midpoints between ordered feature values
                values = np.asarray(list(set(X[:, i])))
                values.sort()
                midpoints = 0.5 * (values[:-1] + values[1:])
                self.possible_tests.append([
                    Test(i, False, x) for x in midpoints
                ])

            if len(values) > 1:
                self.non_constant_features.append(i)

    def random_test(self, rng=None):
        if rng is None:
            rng = np.random.RandomState()
        attr = rng.choice(self.non_constant_features)
        return rng.choice(self.possible_tests[attr])



