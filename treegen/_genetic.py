import tqdm
import numpy as np
from sklearn.metrics import r2_score
from treegen._tree import Tree, Node
from treegen._test import Test, TestGenerator


def tree_evolution(X, Y, max_depth, population_size, generations,
                   nominal_features, allow_subsets, seed, verbose):

    rng = np.random.RandomState(seed)
    test_generator = TestGenerator(X, nominal_features, allow_subsets)
    population = []
    for _ in range(population_size):
        tree = Tree.random_tree(max_depth, test_generator, rng.randint(10**9)) 
        tree.finalize(X, Y)
        population.append(tree)

    for gen in tqdm.tqdm(range(generations), disable=not verbose):
        candidates = []
        for tree in population:
            candidates.append(tree)

            partner = population[np.random.randint(population_size)]
            candidate1 = tree.crossover(partner)
            candidate1.finalize(X, Y)
            candidates.append(candidate1)

            candidate2 = tree.mutate()
            candidate2.finalize(X, Y)
            candidates.append(candidate2)

        scored = [(evaluate_tree(tree, X, Y), tree) for tree in candidates]
        scored.sort(key=lambda x: -x[0])

        population = [tree for _, tree in scored[:population_size]]

    return population


def evaluate_tree(tree, X, y):
    predictions = tree.predict(X)
    return r2_score(y, predictions)

