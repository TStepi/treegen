import numpy as np


class Tree:
    def __init__(self):
        self.root_node = Node()
        self.rng = np.random.RandomState()
        self.out_size = None
        self.test_generator = None

    @staticmethod
    def random_tree(max_depth, test_generator, seed):
        tree = Tree()
        tree.rng = np.random.RandomState(seed)
        tree.root_node = Node.random_tree(max_depth, test_generator, tree.rng)
        tree.test_generator = test_generator
        return tree

    def copy(self, fix_rng=False):
        new_tree = Tree()
        new_tree.rng = np.random.RandomState(self.rng.randint(10**9))
        new_tree.out_size = self.out_size
        new_tree.root_node = self.root_node.copy()
        new_tree.test_generator = self.test_generator
        return new_tree

    def crossover(self, other):
        offspring = self.copy()
        if self.root_node.height > 1:
            parent1, path = offspring.root_node.random_child(self.rng, min_height=2)
            parent2 = other.root_node.get_child(path)
            parent1.right = parent2.right
        return offspring

    def mutate(self):
        mutant = self.copy()
        node, _ = mutant.root_node.random_child(self.rng)
        node.test = self.test_generator.random_test(self.rng)
        return mutant

    def prune(self):
        return self

    def finalize(self, X, Y):
        self.out_size = Y.shape[1]
        self.root_node.finalize(X, Y)
    
    def predict(self, X):
        return self.root_node.predict(X, self.out_size)

    def repr(self, feature_names, target_names):
        return self.root_node.repr(feature_names, target_names)



class Node:

    def __init__(self, depth=0, height=0):
        self.right = None
        self.wrong = None
        self.test = None
        self.prediction = None
        self.total_weight = None
        self.depth = depth
        self.height = height

    def copy(self):
        new_node = Node(self.depth, self.height)
        new_node.right = None if self.right is None else self.right.copy()
        new_node.wrong = None if self.wrong is None else self.wrong.copy()
        new_node.test = self.test
        new_node.prediction = self.prediction
        new_node.total_weight = self.total_weight
        return new_node

    @staticmethod
    def random_tree(max_depth, test_generator, rng, current_depth=0):
        root_node = Node(current_depth,  max_depth-current_depth)
        if max_depth > current_depth:
            root_node.test = test_generator.random_test(rng)
            root_node.right = Node.random_tree(max_depth, test_generator, rng, current_depth+1)
            root_node.wrong = Node.random_tree(max_depth, test_generator, rng, current_depth+1)
        return root_node

    def get_child(self, path):
        if len(path) == 0:
            return self
        elif path[0] == 'right':
            return self.right.get_child(path[1:])
        elif path[0] == 'wrong':
            return self.wrong.get_child(path[1:])
        else:
            raise ValueError(f'Unknown path specification {path[0]}')

    def random_child(self, rng, min_height=1, path=None):
        if path is None:
            path = []
        nodes_left = 2**(self.height - min_height+1)-1
        a = rng.random()
        if a < 1 / nodes_left:
            return self, path
        elif a < (nodes_left + 1) / (2 * nodes_left):
            return self.right.random_child(rng, min_height, path+['right'])
        else:
            return self.wrong.random_child(rng, min_height, path+['wrong'])

    def finalize(self, X, Y, weights=None, freeze=None):
        """
        Set the prototypes in leaves.
        Freze is used for leaves which have no examples - in such cases,
        the prototype is the same that it would be in the last node with
        examples.
        """
        if freeze is not None:
            self.total_weight = 0
        elif weights is None:
            weights = np.ones(X.shape[0])
            self.total_weight = X.shape[0]
        else:
            self.total_weight = weights.sum()

        if self.height == 0:
            if freeze is not None:
                self.prediction = np.average(Y, axis=0, weights=freeze)
            else:
                self.prediction = np.average(Y, axis=0, weights=weights)
        elif freeze is not None:
            self.right.finalize(X, Y, None, freeze)
            self.wrong.finalize(X, Y, None, freeze)
        else:
            test_results = np.apply_along_axis(self.test.apply, 1, X)
            weights_right = weights * test_results
            weights_wrong = weights * (1-test_results)

            if weights_right.sum() == 0:
                # No examples go right, use the prototype from this node
                self.right.finalize(X, Y, None, weights)
            else:
                self.right.finalize(X, Y, weights_right)

            if weights_wrong.sum() == 0:
                # No examples go right, use the prototype from this node
                self.wrong.finalize(X, Y, None, weights)
            else:
                self.wrong.finalize(X, Y, weights_wrong)

    def predict(self, X, out_size):
        if self.height == 0:
            return np.tile(self.prediction, (X.shape[0], 1))
        else:
            test_results = np.apply_along_axis(self.test.apply, 1, X)
            right = test_results > 0
            wrong = test_results < 1
            predictions = np.zeros((X.shape[0], out_size))
            if right.sum() > 0:
                predictions_right = self.right.predict(X[right], out_size)
                predictions[right] += predictions_right
            if wrong.sum() > 0:
                predictions_wrong = self.wrong.predict(X[wrong], out_size)
                predictions[wrong] += predictions_wrong
            predictions[right & wrong] *= 0.5
            return predictions

    def repr(self, feature_names, target_names, prefix=''):
        if self.height == 0:
            temp = list(zip(target_names, self.prediction))
            return f'{prefix} [{self.total_weight}] {temp}'
        else:
            right = self.right.repr(feature_names, target_names, 
                                    ' '*len(prefix)+'YES:')
            wrong = self.wrong.repr(feature_names, target_names,
                                    ' '*len(prefix)+' NO:')
            return f'{prefix} [{self.total_weight}] {self.test.repr(feature_names)}\n{right}\n{wrong}'
