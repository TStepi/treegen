import numpy as np
import pandas as pd
from pandas.api.types import is_numeric_dtype
from treegen._genetic import tree_evolution

DTYPE = 'f'


class Model:

    def __init__(self,
                 max_depth=10,
                 n_jobs=1,
                 generations=100,
                 population_size=100,
                 standardize_targets=True,
                 allow_nominal_subsets=True,
                 verbose=False,
                 seed=None):

        self.max_depth = max_depth
        self.generations = generations
        self.population_size = population_size
        self.standardize_targets = standardize_targets
        self.allow_nominal_subsets = allow_nominal_subsets
        self.n_jobs = n_jobs
        self.verbose = verbose
        self.seed = seed

        # set after fitting
        self.feature_names = None
        self.target_names = None
        # after fitting the model, this holds the final population of trees
        self.trees = None          
        # the tree from the final population that is selected?
        self.used_tree = None  

    def fit(self, X, Y, nominal_features=None, target_weights=None):

        if type(X) != pd.DataFrame or type(Y) != pd.DataFrame:
            raise ValueError(
                'Features and targets should be provided as pandas DataFrames'
            )

        n, d = X.shape
        self.feature_names = X.columns
        self.target_names = Y.columns

        if nominal_features is None:
            nominal_features = []
            for i in range(d):
                if not is_numeric_dtype(X.dtypes[i]):
                    nominal_features.append(i)

        if target_weights is not None:
            target_weights = target_weights.astype(DTYPE, order='C', copy=False)
            target_weights /= target_weights.max()
            self.standardize_targets = True

        # standardize targets here and apply target weights

        if self.seed is None:
            self.seed = np.random.randint(10**9)

        self.trees = tree_evolution(
            X.values, Y.values, self.max_depth, self.population_size, 
            self.generations, nominal_features, self.allow_nominal_subsets, 
            self.seed, self.verbose
        )

        self.pick_tree()

    def pick_tree(self):
        self.used_tree = self.trees[0].copy().prune()

    def predict(self, X):
        return pd.DataFrame(self.used_tree.predict(X), 
                            index=X.index, columns=self.target_names)

    def repr(self):
        return self.used_tree.repr(self.feature_names, self.target_names)
